﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void LPWWaterChunk::.ctor()
extern void LPWWaterChunk__ctor_m320F38BC07ADC09498FFB663E77413E0EDC7FC16 (void);
// 0x00000002 System.Void CameraFollow::Start()
extern void CameraFollow_Start_mC13DAB135378AADB3E72275F75CDEAF53343810D (void);
// 0x00000003 System.Void CameraFollow::Update()
extern void CameraFollow_Update_m9762CC5B7A2D28B7B8116BA8DE2342AFB654AB08 (void);
// 0x00000004 System.Void CameraFollow::LateUpdate()
extern void CameraFollow_LateUpdate_m140D0956A03E1CD6C6DF9B74330BDA2A0813248F (void);
// 0x00000005 System.Void CameraFollow::.ctor()
extern void CameraFollow__ctor_m29F88CCFD2ED12A7BCC75A9BBA892CEF179C83DE (void);
// 0x00000006 System.Void Constants::Awake()
extern void Constants_Awake_m3D6ECA932C50BE5F6EFEB2761BD069E7DD372205 (void);
// 0x00000007 System.Void Constants::.ctor()
extern void Constants__ctor_m0EA1262B0E2FEAFEEA315AD5C2CF86C2B64DF6AF (void);
// 0x00000008 System.Void Enemy::Awake()
extern void Enemy_Awake_mF268033197059561A4A0BC3E5F6B83B50D29C861 (void);
// 0x00000009 System.Void Enemy::Start()
extern void Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02 (void);
// 0x0000000A System.Void Enemy::FixedUpdate()
extern void Enemy_FixedUpdate_m66DA7C35B28A130AF6359CE888D789DC85FF18FE (void);
// 0x0000000B System.Void Enemy::MoveEnemyPlayer()
extern void Enemy_MoveEnemyPlayer_m9FA9B0CDBE5512D59A8ABA58D38AC57922E4A34D (void);
// 0x0000000C System.Void Enemy::OnCollisionEnter(UnityEngine.Collision)
extern void Enemy_OnCollisionEnter_mE8D80BA64A59FF9208672564BF6E8414505C9E35 (void);
// 0x0000000D System.Void Enemy::.ctor()
extern void Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734 (void);
// 0x0000000E System.Void GameManager::Awake()
extern void GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30 (void);
// 0x0000000F System.Void GameManager::Start()
extern void GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E (void);
// 0x00000010 System.Boolean GameManager::PlayerAlive()
extern void GameManager_PlayerAlive_mFAAF0D96DECA4AFF212925CA2A54F9D85E13B382 (void);
// 0x00000011 System.Boolean GameManager::LevelCompleted()
extern void GameManager_LevelCompleted_m6656F4526BD6B523A7AFD44BE2D4DADEA94E6D11 (void);
// 0x00000012 GameStates/States GameManager::get_GameState()
extern void GameManager_get_GameState_m192E95F89DDF5F2C79290CC8D1DB87D9BC697276 (void);
// 0x00000013 System.Void GameManager::set_GameState(GameStates/States)
extern void GameManager_set_GameState_m7277C377F2E3BFF5B1AAE71D85F3BAA414E48708 (void);
// 0x00000014 GameStates/States GameManager::get_GameStart()
extern void GameManager_get_GameStart_mBD6195F7956399E5DC1DFA735FE27A778DF075A7 (void);
// 0x00000015 System.Void GameManager::set_GameStart(GameStates/States)
extern void GameManager_set_GameStart_m90D180A05FCE240D89F78554D28F274C3BE812A9 (void);
// 0x00000016 System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x00000017 System.Void GameStates::Awake()
extern void GameStates_Awake_m04A7FFA021A6381FDCE5E1B70F47913154E4835B (void);
// 0x00000018 System.Void GameStates::.ctor()
extern void GameStates__ctor_mCB8CB7969D9BB43B4CFFE2541EF9156D19B6402E (void);
// 0x00000019 System.Void InputManager::Awake()
extern void InputManager_Awake_m56AAFE728DF63B4B922FC31449C82EC6CFB0D946 (void);
// 0x0000001A System.Void InputManager::Update()
extern void InputManager_Update_mE17FD2A03E0E1BE94DFE0B0AB4B5B9C5F3EA285B (void);
// 0x0000001B UnityEngine.Vector2 InputManager::GetInputValues()
extern void InputManager_GetInputValues_m5F9607A59BEF17F056EB3B22F865906DC8FBC1E5 (void);
// 0x0000001C System.Void InputManager::.ctor()
extern void InputManager__ctor_mB533F16325A793C9274F6CA3804EBCE27AD700A7 (void);
// 0x0000001D System.Void JumpFloor::Awake()
extern void JumpFloor_Awake_m8C5053F6384D1F7767658EE2C81768A54E70C874 (void);
// 0x0000001E System.Void JumpFloor::Start()
extern void JumpFloor_Start_m257843C120702C1C39B4141BCACD1A3849DFA4EB (void);
// 0x0000001F System.Int32 JumpFloor::get_KeyValue()
extern void JumpFloor_get_KeyValue_mB16FB5D8F11ACC85AF832D5A40359312603529AE (void);
// 0x00000020 System.Void JumpFloor::.ctor()
extern void JumpFloor__ctor_m0981BAAD26A3E9AD63BAE8DCEFA4B9C95046436A (void);
// 0x00000021 System.Void JumpFloorSpawner::Awake()
extern void JumpFloorSpawner_Awake_m9A6FC0B48BADC577403504304A07824C459630BA (void);
// 0x00000022 System.Void JumpFloorSpawner::Start()
extern void JumpFloorSpawner_Start_m9B6D70EF9721F434C11F313B5C7E82F02A812097 (void);
// 0x00000023 System.Int32 JumpFloorSpawner::get_GetJumpFloorKey()
extern void JumpFloorSpawner_get_GetJumpFloorKey_m0DDE290ADD1DE2545433AE87043FBA951931D2DA (void);
// 0x00000024 System.Void JumpFloorSpawner::GenrateJumpFloors()
extern void JumpFloorSpawner_GenrateJumpFloors_mEDB7F4EADEC20EAF554ED259AA191B4A99F0FBC7 (void);
// 0x00000025 System.Void JumpFloorSpawner::GenrateFloor(UnityEngine.GameObject,System.Int32)
extern void JumpFloorSpawner_GenrateFloor_mC3516FDA41608375A9B7A9642FFFE53ACBE9AE23 (void);
// 0x00000026 System.Void JumpFloorSpawner::.ctor()
extern void JumpFloorSpawner__ctor_mC0C2A4CE0DC42541692099F756F56C53A0966AFC (void);
// 0x00000027 System.Void Laser::Start()
extern void Laser_Start_m2ED780B1D3554CE1EA69A58AAEBC8720D284D9CD (void);
// 0x00000028 System.Void Laser::Update()
extern void Laser_Update_mF7AF88BE6F4EEB42638D921A1B4F818279B64957 (void);
// 0x00000029 System.Void Laser::.ctor()
extern void Laser__ctor_mEE165BF77F7AC80E6261725C4A19E12DA1D64781 (void);
// 0x0000002A System.Void Player::Start()
extern void Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021 (void);
// 0x0000002B System.Void Player::Update()
extern void Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3 (void);
// 0x0000002C System.Void Player::FixedUpdate()
extern void Player_FixedUpdate_mD7447EDFC86F29A3E5FBDEF7E0139535BD4C5088 (void);
// 0x0000002D System.Void Player::MovePlayer()
extern void Player_MovePlayer_mBE1240CDB20CA9B43956A54F7164BC24F755FF68 (void);
// 0x0000002E System.Void Player::OnCollisionEnter(UnityEngine.Collision)
extern void Player_OnCollisionEnter_mC2785602469BC029A5E804A22D83459ECE1C42BE (void);
// 0x0000002F System.Void Player::.ctor()
extern void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (void);
// 0x00000030 System.Void PlayerNameGenrator::Awake()
extern void PlayerNameGenrator_Awake_m1D97F43E6C280866E1480004D8E4AE6FFECAC874 (void);
// 0x00000031 System.String PlayerNameGenrator::get_GetPlayerName()
extern void PlayerNameGenrator_get_GetPlayerName_mD039A07A5F1E60D11F70F093E0469E30017B5F14 (void);
// 0x00000032 System.String PlayerNameGenrator::get_PlayerName()
extern void PlayerNameGenrator_get_PlayerName_mF58ACC21EDD50A272C25A5D53E04E6226E6463A1 (void);
// 0x00000033 System.Void PlayerNameGenrator::.ctor()
extern void PlayerNameGenrator__ctor_mBE417C31C0669C25CB6B75BEF8445A4AF67EF905 (void);
// 0x00000034 System.Void PlayerSpawner::Awake()
extern void PlayerSpawner_Awake_m6E649CFB730D8E63CEF603B16D085B8FCDE11DA0 (void);
// 0x00000035 System.Void PlayerSpawner::Start()
extern void PlayerSpawner_Start_m57B4A2B73E54EE0BB0E3D4D37733987191040910 (void);
// 0x00000036 System.Void PlayerSpawner::SpawnPlayer(UnityEngine.GameObject,UnityEngine.Vector3)
extern void PlayerSpawner_SpawnPlayer_m8496CE5BC43F2A013ACEC80A1735142FEF44B457 (void);
// 0x00000037 System.Void PlayerSpawner::.ctor()
extern void PlayerSpawner__ctor_m5485E8757E4F2794DD7F455FB72ED58290CC21EA (void);
// 0x00000038 System.Void RankManager::Awake()
extern void RankManager_Awake_m3C5FEE64D2FBC57E59D89674DED5FB2EBB52159E (void);
// 0x00000039 System.Int32 RankManager::get_getPlayerCount()
extern void RankManager_get_getPlayerCount_m292ED88495D67F693883486BF0130B1911A170A7 (void);
// 0x0000003A System.Void RankManager::AddInitialRank(System.String,System.Int32)
extern void RankManager_AddInitialRank_mD9DC5792BC834C92716B3BCB54AE5FA65BE366C8 (void);
// 0x0000003B System.Void RankManager::UpdateRank(System.String,System.Int32)
extern void RankManager_UpdateRank_m91AC9F275E743E9E17A8D616A22745C965C757B7 (void);
// 0x0000003C System.String[] RankManager::GetPlayersName()
extern void RankManager_GetPlayersName_mAFC121C5E512475DFED75E866C5C8847B0DE1DEE (void);
// 0x0000003D System.Void RankManager::.ctor()
extern void RankManager__ctor_mABC316C9D683BDCF250052EF5C2FAD05EAD53377 (void);
// 0x0000003E System.Void RankManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m599B83098452CF142A0EEE4B24BD6E55AE663611 (void);
// 0x0000003F System.Void RankManager/<>c::.ctor()
extern void U3CU3Ec__ctor_m4FB9589DF1B865EBE164EDBECF67D2E53B726C0D (void);
// 0x00000040 System.Int32 RankManager/<>c::<GetPlayersName>b__8_0(System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>)
extern void U3CU3Ec_U3CGetPlayersNameU3Eb__8_0_mD563AB6CFC9658A01D68C43685CCA64B33EFBA0B (void);
// 0x00000041 System.Void UIManager::Awake()
extern void UIManager_Awake_mCED93604270B1E209B4E0D32F6A26DDC5AB06E30 (void);
// 0x00000042 System.Void UIManager::Start()
extern void UIManager_Start_mAA4B371DC406146E84A9D8803B9C861939B2E04E (void);
// 0x00000043 System.Void UIManager::DeActivateAll()
extern void UIManager_DeActivateAll_mAA4141491C657DECBB3D38C7FAE19EA4A8A69616 (void);
// 0x00000044 System.Void UIManager::ShowGameOver()
extern void UIManager_ShowGameOver_m4B55C013EA5C3E049C1D2587A9A4D3C163AD3FBC (void);
// 0x00000045 System.Void UIManager::UpdatePlayerRanks()
extern void UIManager_UpdatePlayerRanks_m11562730387A5F9B5EA423FF33235484B964EC8C (void);
// 0x00000046 System.Void UIManager::LevelCompleedByPlayer()
extern void UIManager_LevelCompleedByPlayer_mB42FA357F4F9E0937957FCAD29E63704B8FF42B1 (void);
// 0x00000047 System.Void UIManager::.ctor()
extern void UIManager__ctor_mDADE1D724D40AF63AE78D51FC1CF1FE4784B4D4B (void);
// 0x00000048 System.Void UIManager::<Start>b__10_0()
extern void UIManager_U3CStartU3Eb__10_0_m0F692721EFA327299E8E5977074886132C534DB2 (void);
// 0x00000049 System.Void UIManager/<>c::.cctor()
extern void U3CU3Ec__cctor_mBA5A8E7D4C803C8F80B71126C81B4760B987AD2A (void);
// 0x0000004A System.Void UIManager/<>c::.ctor()
extern void U3CU3Ec__ctor_m0A79C1A3BEA9231B3E275AEE65DA7EE93B137E15 (void);
// 0x0000004B System.Void UIManager/<>c::<Start>b__10_1()
extern void U3CU3Ec_U3CStartU3Eb__10_1_m7E4C6D7C807A028D4BCC0601CB1AA8DA2D44175A (void);
// 0x0000004C System.Void LPWAsset.LowPolyWaterScript::OnEnable()
extern void LowPolyWaterScript_OnEnable_mA5066F6F89F78C9D23F35D1CC437DBC046A0FA0D (void);
// 0x0000004D System.Void LPWAsset.LowPolyWaterScript::Update()
extern void LowPolyWaterScript_Update_mE7A061C9ABDE62A956E8CC4B4314E1E7C86FEA67 (void);
// 0x0000004E System.Void LPWAsset.LowPolyWaterScript::OnDisable()
extern void LowPolyWaterScript_OnDisable_mB36D6C91ED3D04B6FD63A0C7BF3DCF9A1507B2C1 (void);
// 0x0000004F System.Void LPWAsset.LowPolyWaterScript::OnDestroy()
extern void LowPolyWaterScript_OnDestroy_mCBF26D46DA4CF0D9A590FC6B06A1AD044EB250E2 (void);
// 0x00000050 System.Void LPWAsset.LowPolyWaterScript::CleanUp(System.Boolean)
extern void LowPolyWaterScript_CleanUp_m0F640ACD5434AFEB46123427693EA55B5846D75B (void);
// 0x00000051 System.Void LPWAsset.LowPolyWaterScript::Destroy_(UnityEngine.Object)
extern void LowPolyWaterScript_Destroy__mAE793363F8730D18C865D70F690505F847F1B6E2 (void);
// 0x00000052 System.Void LPWAsset.LowPolyWaterScript::Generate()
extern void LowPolyWaterScript_Generate_m6BE2A6E6D43A240AFDEDDD6F7D0A0012F873E6B8 (void);
// 0x00000053 System.Single LPWAsset.LowPolyWaterScript::Encode(UnityEngine.Vector3)
extern void LowPolyWaterScript_Encode_m0DA4D8FA98802BCC39C9F8394585CFB8C1A311E3 (void);
// 0x00000054 System.Void LPWAsset.LowPolyWaterScript::BakeCustomMesh(UnityEngine.Mesh,System.Single)
extern void LowPolyWaterScript_BakeCustomMesh_mF0F3C0B12C2DE0C99E3D7E635C45C1D7273E8648 (void);
// 0x00000055 System.Void LPWAsset.LowPolyWaterScript::BakeMesh(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<System.Int32>,System.Single)
extern void LowPolyWaterScript_BakeMesh_mA474967B8D003DAF7E6C85E85B3EB00C3D907229 (void);
// 0x00000056 System.Void LPWAsset.LowPolyWaterScript::BakeUVs(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.MeshFilter>)
extern void LowPolyWaterScript_BakeUVs_m3BE49073A34A9ACDDF03EE0300C9BF66FDC8A99B (void);
// 0x00000057 System.Void LPWAsset.LowPolyWaterScript::BakeUVsV4(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.MeshFilter>)
extern void LowPolyWaterScript_BakeUVsV4_mA14989C87B3AA727E1EF103E9DD7AF7B3DD8AA1F (void);
// 0x00000058 UnityEngine.Vector3 LPWAsset.LowPolyWaterScript::ApplyLOD(UnityEngine.Vector3,System.Single)
extern void LowPolyWaterScript_ApplyLOD_mD81CC29EC76CE399E05627A837F9803DF6CA24A7 (void);
// 0x00000059 UnityEngine.Vector3 LPWAsset.LowPolyWaterScript::AddNoise(UnityEngine.Vector3)
extern void LowPolyWaterScript_AddNoise_m6117AC6C7F7860F29DE676DB39378476B6B1C73D (void);
// 0x0000005A System.Void LPWAsset.LowPolyWaterScript::Add(System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Vector3,System.Single)
extern void LowPolyWaterScript_Add_m7EDB17111AF55DFD441F133A8CB0AADF40BB7D78 (void);
// 0x0000005B System.Void LPWAsset.LowPolyWaterScript::GenerateSquare()
extern void LowPolyWaterScript_GenerateSquare_mAEF9259DD76F525A5809161E254B64E89A2B74E4 (void);
// 0x0000005C System.Void LPWAsset.LowPolyWaterScript::GenerateHexagonal()
extern void LowPolyWaterScript_GenerateHexagonal_m427139130521699FC1B6440C36CCCB92CDCBD4FE (void);
// 0x0000005D System.Void LPWAsset.LowPolyWaterScript::.ctor()
extern void LowPolyWaterScript__ctor_m47AF193E09944684B36640F4E8A1DE90AD492A9A (void);
// 0x0000005E System.Void LPWAsset.LowPolyWaterScript::.cctor()
extern void LowPolyWaterScript__cctor_m1925A480C09721B7F00EA2AEF7D43B0440FD2804 (void);
// 0x0000005F System.Void LPWAsset.LPWDepthEffect::Init(System.Boolean)
extern void LPWDepthEffect_Init_m5E069C8D139F2E8F2A5BDA03E8E23B1B5B36384C (void);
// 0x00000060 System.Void LPWAsset.LPWDepthEffect::OnWillRenderObject()
extern void LPWDepthEffect_OnWillRenderObject_mA93D070A8A957FAE97BD7225ECFDBBEC07069A48 (void);
// 0x00000061 System.Void LPWAsset.LPWDepthEffect::OnDisable()
extern void LPWDepthEffect_OnDisable_mB3DD71F83EFD0FD2F07987187CC8A7AA54D09762 (void);
// 0x00000062 System.Void LPWAsset.LPWDepthEffect::Destroy_(UnityEngine.Object)
extern void LPWDepthEffect_Destroy__mDB07E7A94205B6CEEA4D06B64CD70032590D37B5 (void);
// 0x00000063 System.Void LPWAsset.LPWDepthEffect::.ctor()
extern void LPWDepthEffect__ctor_mEA2A347E56A05B4F5290DE4908A8C2912B348FC4 (void);
// 0x00000064 System.Void LPWAsset.LPWDepthEffect::.cctor()
extern void LPWDepthEffect__cctor_m4B400458F15A17C717F898A81ED623C9B3520393 (void);
// 0x00000065 System.Single LPWAsset.Displacement::Get(UnityEngine.Vector3,UnityEngine.Material)
extern void Displacement_Get_mF1F7C12E32060A802911B9ADC8A5223DAE0D3B8D (void);
// 0x00000066 System.Single LPWAsset.Displacement::noise(UnityEngine.Vector2)
extern void Displacement_noise_mF7124244BE85C10E01436DE197CAB34BD000F240 (void);
// 0x00000067 System.Single LPWAsset.Displacement::noiseLQ(UnityEngine.Vector2)
extern void Displacement_noiseLQ_m5D691490923EC7ED91E8EDF7FC5F9C67172924AF (void);
// 0x00000068 System.Void LPWAsset.Displacement::gerstner(UnityEngine.Vector3&,System.Single)
extern void Displacement_gerstner_m599BB8872FC7228F806C7894F5AF7D6C6C8342BB (void);
// 0x00000069 System.Single LPWAsset.Displacement::ripple(UnityEngine.Vector2,System.Single)
extern void Displacement_ripple_m48DF4212ADECB3D90D356AAC6668E1657312CAB3 (void);
// 0x0000006A System.Single LPWAsset.Displacement::hash(System.Single)
extern void Displacement_hash_m9E3AF096343E3311D488D93CDC619C74F5CB3915 (void);
// 0x0000006B System.Single LPWAsset.Displacement::frac(System.Single)
extern void Displacement_frac_m9EDD4A8CBCC2E0854C765C8A680AEE9B8BC54BF4 (void);
// 0x0000006C UnityEngine.Vector2 LPWAsset.Displacement::frac(UnityEngine.Vector2)
extern void Displacement_frac_mF30C555F50E8B656638622612685623E71487A66 (void);
// 0x0000006D System.Single LPWAsset.Displacement::floor(System.Single)
extern void Displacement_floor_m05E60E7307135D9E5FCC8DAFD120A39DD2C69A95 (void);
// 0x0000006E UnityEngine.Vector2 LPWAsset.Displacement::floor(UnityEngine.Vector2)
extern void Displacement_floor_m820395F03250246D08AB9B32801FAB4A1B494E9A (void);
// 0x0000006F System.Single LPWAsset.LPWNoise::GetValue(System.Single,System.Single)
extern void LPWNoise_GetValue_mE4540CFD30D6E693E7BD7A43F8B5E36E8153CA08 (void);
// 0x00000070 System.Single LPWAsset.LPWNoise::Hash(System.Int32,System.Int32)
extern void LPWNoise_Hash_mACC4B9E998D4006EE7BE69BB1699E5C7E5329FD4 (void);
// 0x00000071 System.Void LPWAsset.LPWNormalSolver::.ctor()
extern void LPWNormalSolver__ctor_m280CA574750BD6C49EE08E5CEBB09FA43641C6F4 (void);
// 0x00000072 System.Void LPWAsset.LPWNormalSolver::Recalculate(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<System.Int32>)
extern void LPWNormalSolver_Recalculate_m16BA24FD8AE39B5EF4B82415802EE295DC27F7AB (void);
// 0x00000073 System.Boolean LPWAsset.LPWNormalSolver::EqualApprox(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LPWNormalSolver_EqualApprox_m1BF08E96065C5F8F56BBA9DC5A23B2BA4EBB01D4 (void);
// 0x00000074 System.Void LPWAsset.LPWNormalSolver::AddData(UnityEngine.Vector3,System.Int32,System.Int32)
extern void LPWNormalSolver_AddData_mBF5492E1D03DCECB1EE062A67A6CE601BDF472C4 (void);
// 0x00000075 System.Void LPWAsset.LPWNormalSolver::AddPoint(UnityEngine.Vector3)
extern void LPWNormalSolver_AddPoint_m784B89ED7858494A9D3A193D6E2B012831DE1877 (void);
// 0x00000076 System.Void LPWAsset.LPWNormalSolver/LPWPointDataEntry::.ctor(System.Int32,System.Int32)
extern void LPWPointDataEntry__ctor_m7D67D5B1C47F2585B63748B67013C0B6AFD038B8 (void);
// 0x00000077 System.Void LPWAsset.LPWNormalSolver/LPWPoint::.ctor(System.Int32,System.Int32)
extern void LPWPoint__ctor_m5E48D160398799B0900A51FC3D0BB606881FE745 (void);
// 0x00000078 System.Void LPWAsset.LPWNormalSolver/LPWPair::.ctor(System.Int32,System.Int32)
extern void LPWPair__ctor_m246F718705CD2B394F03811F0763F093146167E7 (void);
// 0x00000079 System.Int32 LPWAsset.LPWNormalSolver/LPWPair::GetHashCode()
extern void LPWPair_GetHashCode_mDFB5FBECB18F4A9AAD9C0475D6B0B7432D4847F5 (void);
// 0x0000007A System.Boolean LPWAsset.LPWNormalSolver/LPWPair::Equals(LPWAsset.LPWNormalSolver/LPWPair)
extern void LPWPair_Equals_mD4DEDB9594047E251B433F503998D270AA8792D0 (void);
// 0x0000007B System.Void LPWAsset.LPWNormalSolver/LPWPosition::.ctor(UnityEngine.Vector3)
extern void LPWPosition__ctor_m8662EC582EBF81A2754433D0F7180284C3C09B8E (void);
// 0x0000007C System.Int32 LPWAsset.LPWNormalSolver/LPWPosition::GetHashCode()
extern void LPWPosition_GetHashCode_mFCCAE9479C211673BB8B64A152D7318A2920836C (void);
// 0x0000007D System.Boolean LPWAsset.LPWNormalSolver/LPWPosition::Equals(LPWAsset.LPWNormalSolver/LPWPosition)
extern void LPWPosition_Equals_mD3AF450217DA095D13C93D58FBC129C0DAF76944 (void);
// 0x0000007E System.Void LPWAsset.LPWReflectionParams::.ctor()
extern void LPWReflectionParams__ctor_mF94FDFEA1B521D53A79694C4884BDF0F32F0D435 (void);
// 0x0000007F System.Void LPWAsset.LPWReflection::Init(LPWAsset.LPWReflectionParams,System.Boolean,System.Boolean)
extern void LPWReflection_Init_mFDE7AF47086F5B9A78E2210C028BB9C9A2179CDD (void);
// 0x00000080 System.Void LPWAsset.LPWReflection::OnWillRenderObject()
extern void LPWReflection_OnWillRenderObject_mB3B075BB0BE6078E478A2081610DD7686830CA19 (void);
// 0x00000081 System.Boolean LPWAsset.LPWReflection::IsSceneView2D()
extern void LPWReflection_IsSceneView2D_m6915790A4FEF18A1E86FB7D13087E332EFBB073E (void);
// 0x00000082 System.Void LPWAsset.LPWReflection::OnDisable()
extern void LPWReflection_OnDisable_m4CE558F6D5253A071DFF0D5C9540F1A55D9592FB (void);
// 0x00000083 System.Void LPWAsset.LPWReflection::Destroy_(UnityEngine.Object)
extern void LPWReflection_Destroy__m6CC10B4FEC92C46A44B04DDCDAFC5D8490BACA2B (void);
// 0x00000084 System.Void LPWAsset.LPWReflection::UpdateCameraModes(UnityEngine.Camera,UnityEngine.Camera)
extern void LPWReflection_UpdateCameraModes_m40B5044A989124D8A48E1C8B832ADCE74DF7DCBD (void);
// 0x00000085 System.Void LPWAsset.LPWReflection::CreateWaterObjects(UnityEngine.Camera,UnityEngine.Camera&,UnityEngine.Camera&)
extern void LPWReflection_CreateWaterObjects_m6C8DBCFB8FA580F13E37968BEA446555AC5A3640 (void);
// 0x00000086 LPWAsset.WaterMode LPWAsset.LPWReflection::GetWaterMode()
extern void LPWReflection_GetWaterMode_m46B583D4F8353059BB8AEEFEF7DD3B8143BF04D3 (void);
// 0x00000087 LPWAsset.WaterMode LPWAsset.LPWReflection::FindHardwareWaterSupport()
extern void LPWReflection_FindHardwareWaterSupport_mBE532D268454936CF7B9BAAB1746B7A8893E2E16 (void);
// 0x00000088 UnityEngine.Vector4 LPWAsset.LPWReflection::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LPWReflection_CameraSpacePlane_m4AAA424BD7EEAB69CDAB69F9B9673825D469C409 (void);
// 0x00000089 System.Void LPWAsset.LPWReflection::CalculateReflectionMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern void LPWReflection_CalculateReflectionMatrix_m3B97D655D0C89BCC5C7139E223362E1E2C1C67F9 (void);
// 0x0000008A System.Void LPWAsset.LPWReflection::.ctor()
extern void LPWReflection__ctor_m24834C7DF50FD6F643B16EB0549979340D15F367 (void);
// 0x0000008B System.Void LPWAsset.LPWReflection::.cctor()
extern void LPWReflection__cctor_m08775F54D0A64635B698B175248303F1803D2A32 (void);
// 0x0000008C System.Void LowPolyWaterv2Demo.DemoFreeFlyCamera::Start()
extern void DemoFreeFlyCamera_Start_mCC3C8E63A79AF0F140FE20112B2664D2EC4CCBE3 (void);
// 0x0000008D System.Void LowPolyWaterv2Demo.DemoFreeFlyCamera::Update()
extern void DemoFreeFlyCamera_Update_m346AF785CC2FC87D9BBC7B7DCCE669D3C5ADE49A (void);
// 0x0000008E System.Void LowPolyWaterv2Demo.DemoFreeFlyCamera::.ctor()
extern void DemoFreeFlyCamera__ctor_m36640884A06AC159ACB750CF32C92DA40AA74D26 (void);
// 0x0000008F System.Void EpicToonFX.ETFXLightFade::Start()
extern void ETFXLightFade_Start_m80EEDD11315E813676DA8E550C806F8C96374733 (void);
// 0x00000090 System.Void EpicToonFX.ETFXLightFade::Update()
extern void ETFXLightFade_Update_m3705B92646C8612BC70987DF39BC94B8F88EC49D (void);
// 0x00000091 System.Void EpicToonFX.ETFXLightFade::.ctor()
extern void ETFXLightFade__ctor_mCDF41DC7334BC08F86DD3EF815FC1F3EA4C44F11 (void);
// 0x00000092 System.Void EpicToonFX.ETFXPitchRandomizer::Start()
extern void ETFXPitchRandomizer_Start_m64BE3B77C622908C2E324C6D3DFD17CB9092D21E (void);
// 0x00000093 System.Void EpicToonFX.ETFXPitchRandomizer::.ctor()
extern void ETFXPitchRandomizer__ctor_m8493E0829884B225C1FC98134FA1EFCC44CDD238 (void);
// 0x00000094 System.Void EpicToonFX.ETFXRotation::Start()
extern void ETFXRotation_Start_m3DC21DA9C26DBA1E14C27B9B997F46BEBBFB22C7 (void);
// 0x00000095 System.Void EpicToonFX.ETFXRotation::Update()
extern void ETFXRotation_Update_mED37E3F435A6166C540A1DA30B49354F05279100 (void);
// 0x00000096 System.Void EpicToonFX.ETFXRotation::.ctor()
extern void ETFXRotation__ctor_mC1C7849D64611953B026F069EE879C10B1399ABA (void);
static Il2CppMethodPointer s_methodPointers[150] = 
{
	LPWWaterChunk__ctor_m320F38BC07ADC09498FFB663E77413E0EDC7FC16,
	CameraFollow_Start_mC13DAB135378AADB3E72275F75CDEAF53343810D,
	CameraFollow_Update_m9762CC5B7A2D28B7B8116BA8DE2342AFB654AB08,
	CameraFollow_LateUpdate_m140D0956A03E1CD6C6DF9B74330BDA2A0813248F,
	CameraFollow__ctor_m29F88CCFD2ED12A7BCC75A9BBA892CEF179C83DE,
	Constants_Awake_m3D6ECA932C50BE5F6EFEB2761BD069E7DD372205,
	Constants__ctor_m0EA1262B0E2FEAFEEA315AD5C2CF86C2B64DF6AF,
	Enemy_Awake_mF268033197059561A4A0BC3E5F6B83B50D29C861,
	Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02,
	Enemy_FixedUpdate_m66DA7C35B28A130AF6359CE888D789DC85FF18FE,
	Enemy_MoveEnemyPlayer_m9FA9B0CDBE5512D59A8ABA58D38AC57922E4A34D,
	Enemy_OnCollisionEnter_mE8D80BA64A59FF9208672564BF6E8414505C9E35,
	Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734,
	GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30,
	GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E,
	GameManager_PlayerAlive_mFAAF0D96DECA4AFF212925CA2A54F9D85E13B382,
	GameManager_LevelCompleted_m6656F4526BD6B523A7AFD44BE2D4DADEA94E6D11,
	GameManager_get_GameState_m192E95F89DDF5F2C79290CC8D1DB87D9BC697276,
	GameManager_set_GameState_m7277C377F2E3BFF5B1AAE71D85F3BAA414E48708,
	GameManager_get_GameStart_mBD6195F7956399E5DC1DFA735FE27A778DF075A7,
	GameManager_set_GameStart_m90D180A05FCE240D89F78554D28F274C3BE812A9,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	GameStates_Awake_m04A7FFA021A6381FDCE5E1B70F47913154E4835B,
	GameStates__ctor_mCB8CB7969D9BB43B4CFFE2541EF9156D19B6402E,
	InputManager_Awake_m56AAFE728DF63B4B922FC31449C82EC6CFB0D946,
	InputManager_Update_mE17FD2A03E0E1BE94DFE0B0AB4B5B9C5F3EA285B,
	InputManager_GetInputValues_m5F9607A59BEF17F056EB3B22F865906DC8FBC1E5,
	InputManager__ctor_mB533F16325A793C9274F6CA3804EBCE27AD700A7,
	JumpFloor_Awake_m8C5053F6384D1F7767658EE2C81768A54E70C874,
	JumpFloor_Start_m257843C120702C1C39B4141BCACD1A3849DFA4EB,
	JumpFloor_get_KeyValue_mB16FB5D8F11ACC85AF832D5A40359312603529AE,
	JumpFloor__ctor_m0981BAAD26A3E9AD63BAE8DCEFA4B9C95046436A,
	JumpFloorSpawner_Awake_m9A6FC0B48BADC577403504304A07824C459630BA,
	JumpFloorSpawner_Start_m9B6D70EF9721F434C11F313B5C7E82F02A812097,
	JumpFloorSpawner_get_GetJumpFloorKey_m0DDE290ADD1DE2545433AE87043FBA951931D2DA,
	JumpFloorSpawner_GenrateJumpFloors_mEDB7F4EADEC20EAF554ED259AA191B4A99F0FBC7,
	JumpFloorSpawner_GenrateFloor_mC3516FDA41608375A9B7A9642FFFE53ACBE9AE23,
	JumpFloorSpawner__ctor_mC0C2A4CE0DC42541692099F756F56C53A0966AFC,
	Laser_Start_m2ED780B1D3554CE1EA69A58AAEBC8720D284D9CD,
	Laser_Update_mF7AF88BE6F4EEB42638D921A1B4F818279B64957,
	Laser__ctor_mEE165BF77F7AC80E6261725C4A19E12DA1D64781,
	Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021,
	Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3,
	Player_FixedUpdate_mD7447EDFC86F29A3E5FBDEF7E0139535BD4C5088,
	Player_MovePlayer_mBE1240CDB20CA9B43956A54F7164BC24F755FF68,
	Player_OnCollisionEnter_mC2785602469BC029A5E804A22D83459ECE1C42BE,
	Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7,
	PlayerNameGenrator_Awake_m1D97F43E6C280866E1480004D8E4AE6FFECAC874,
	PlayerNameGenrator_get_GetPlayerName_mD039A07A5F1E60D11F70F093E0469E30017B5F14,
	PlayerNameGenrator_get_PlayerName_mF58ACC21EDD50A272C25A5D53E04E6226E6463A1,
	PlayerNameGenrator__ctor_mBE417C31C0669C25CB6B75BEF8445A4AF67EF905,
	PlayerSpawner_Awake_m6E649CFB730D8E63CEF603B16D085B8FCDE11DA0,
	PlayerSpawner_Start_m57B4A2B73E54EE0BB0E3D4D37733987191040910,
	PlayerSpawner_SpawnPlayer_m8496CE5BC43F2A013ACEC80A1735142FEF44B457,
	PlayerSpawner__ctor_m5485E8757E4F2794DD7F455FB72ED58290CC21EA,
	RankManager_Awake_m3C5FEE64D2FBC57E59D89674DED5FB2EBB52159E,
	RankManager_get_getPlayerCount_m292ED88495D67F693883486BF0130B1911A170A7,
	RankManager_AddInitialRank_mD9DC5792BC834C92716B3BCB54AE5FA65BE366C8,
	RankManager_UpdateRank_m91AC9F275E743E9E17A8D616A22745C965C757B7,
	RankManager_GetPlayersName_mAFC121C5E512475DFED75E866C5C8847B0DE1DEE,
	RankManager__ctor_mABC316C9D683BDCF250052EF5C2FAD05EAD53377,
	U3CU3Ec__cctor_m599B83098452CF142A0EEE4B24BD6E55AE663611,
	U3CU3Ec__ctor_m4FB9589DF1B865EBE164EDBECF67D2E53B726C0D,
	U3CU3Ec_U3CGetPlayersNameU3Eb__8_0_mD563AB6CFC9658A01D68C43685CCA64B33EFBA0B,
	UIManager_Awake_mCED93604270B1E209B4E0D32F6A26DDC5AB06E30,
	UIManager_Start_mAA4B371DC406146E84A9D8803B9C861939B2E04E,
	UIManager_DeActivateAll_mAA4141491C657DECBB3D38C7FAE19EA4A8A69616,
	UIManager_ShowGameOver_m4B55C013EA5C3E049C1D2587A9A4D3C163AD3FBC,
	UIManager_UpdatePlayerRanks_m11562730387A5F9B5EA423FF33235484B964EC8C,
	UIManager_LevelCompleedByPlayer_mB42FA357F4F9E0937957FCAD29E63704B8FF42B1,
	UIManager__ctor_mDADE1D724D40AF63AE78D51FC1CF1FE4784B4D4B,
	UIManager_U3CStartU3Eb__10_0_m0F692721EFA327299E8E5977074886132C534DB2,
	U3CU3Ec__cctor_mBA5A8E7D4C803C8F80B71126C81B4760B987AD2A,
	U3CU3Ec__ctor_m0A79C1A3BEA9231B3E275AEE65DA7EE93B137E15,
	U3CU3Ec_U3CStartU3Eb__10_1_m7E4C6D7C807A028D4BCC0601CB1AA8DA2D44175A,
	LowPolyWaterScript_OnEnable_mA5066F6F89F78C9D23F35D1CC437DBC046A0FA0D,
	LowPolyWaterScript_Update_mE7A061C9ABDE62A956E8CC4B4314E1E7C86FEA67,
	LowPolyWaterScript_OnDisable_mB36D6C91ED3D04B6FD63A0C7BF3DCF9A1507B2C1,
	LowPolyWaterScript_OnDestroy_mCBF26D46DA4CF0D9A590FC6B06A1AD044EB250E2,
	LowPolyWaterScript_CleanUp_m0F640ACD5434AFEB46123427693EA55B5846D75B,
	LowPolyWaterScript_Destroy__mAE793363F8730D18C865D70F690505F847F1B6E2,
	LowPolyWaterScript_Generate_m6BE2A6E6D43A240AFDEDDD6F7D0A0012F873E6B8,
	LowPolyWaterScript_Encode_m0DA4D8FA98802BCC39C9F8394585CFB8C1A311E3,
	LowPolyWaterScript_BakeCustomMesh_mF0F3C0B12C2DE0C99E3D7E635C45C1D7273E8648,
	LowPolyWaterScript_BakeMesh_mA474967B8D003DAF7E6C85E85B3EB00C3D907229,
	LowPolyWaterScript_BakeUVs_m3BE49073A34A9ACDDF03EE0300C9BF66FDC8A99B,
	LowPolyWaterScript_BakeUVsV4_mA14989C87B3AA727E1EF103E9DD7AF7B3DD8AA1F,
	LowPolyWaterScript_ApplyLOD_mD81CC29EC76CE399E05627A837F9803DF6CA24A7,
	LowPolyWaterScript_AddNoise_m6117AC6C7F7860F29DE676DB39378476B6B1C73D,
	LowPolyWaterScript_Add_m7EDB17111AF55DFD441F133A8CB0AADF40BB7D78,
	LowPolyWaterScript_GenerateSquare_mAEF9259DD76F525A5809161E254B64E89A2B74E4,
	LowPolyWaterScript_GenerateHexagonal_m427139130521699FC1B6440C36CCCB92CDCBD4FE,
	LowPolyWaterScript__ctor_m47AF193E09944684B36640F4E8A1DE90AD492A9A,
	LowPolyWaterScript__cctor_m1925A480C09721B7F00EA2AEF7D43B0440FD2804,
	LPWDepthEffect_Init_m5E069C8D139F2E8F2A5BDA03E8E23B1B5B36384C,
	LPWDepthEffect_OnWillRenderObject_mA93D070A8A957FAE97BD7225ECFDBBEC07069A48,
	LPWDepthEffect_OnDisable_mB3DD71F83EFD0FD2F07987187CC8A7AA54D09762,
	LPWDepthEffect_Destroy__mDB07E7A94205B6CEEA4D06B64CD70032590D37B5,
	LPWDepthEffect__ctor_mEA2A347E56A05B4F5290DE4908A8C2912B348FC4,
	LPWDepthEffect__cctor_m4B400458F15A17C717F898A81ED623C9B3520393,
	Displacement_Get_mF1F7C12E32060A802911B9ADC8A5223DAE0D3B8D,
	Displacement_noise_mF7124244BE85C10E01436DE197CAB34BD000F240,
	Displacement_noiseLQ_m5D691490923EC7ED91E8EDF7FC5F9C67172924AF,
	Displacement_gerstner_m599BB8872FC7228F806C7894F5AF7D6C6C8342BB,
	Displacement_ripple_m48DF4212ADECB3D90D356AAC6668E1657312CAB3,
	Displacement_hash_m9E3AF096343E3311D488D93CDC619C74F5CB3915,
	Displacement_frac_m9EDD4A8CBCC2E0854C765C8A680AEE9B8BC54BF4,
	Displacement_frac_mF30C555F50E8B656638622612685623E71487A66,
	Displacement_floor_m05E60E7307135D9E5FCC8DAFD120A39DD2C69A95,
	Displacement_floor_m820395F03250246D08AB9B32801FAB4A1B494E9A,
	LPWNoise_GetValue_mE4540CFD30D6E693E7BD7A43F8B5E36E8153CA08,
	LPWNoise_Hash_mACC4B9E998D4006EE7BE69BB1699E5C7E5329FD4,
	LPWNormalSolver__ctor_m280CA574750BD6C49EE08E5CEBB09FA43641C6F4,
	LPWNormalSolver_Recalculate_m16BA24FD8AE39B5EF4B82415802EE295DC27F7AB,
	LPWNormalSolver_EqualApprox_m1BF08E96065C5F8F56BBA9DC5A23B2BA4EBB01D4,
	LPWNormalSolver_AddData_mBF5492E1D03DCECB1EE062A67A6CE601BDF472C4,
	LPWNormalSolver_AddPoint_m784B89ED7858494A9D3A193D6E2B012831DE1877,
	LPWPointDataEntry__ctor_m7D67D5B1C47F2585B63748B67013C0B6AFD038B8,
	LPWPoint__ctor_m5E48D160398799B0900A51FC3D0BB606881FE745,
	LPWPair__ctor_m246F718705CD2B394F03811F0763F093146167E7,
	LPWPair_GetHashCode_mDFB5FBECB18F4A9AAD9C0475D6B0B7432D4847F5,
	LPWPair_Equals_mD4DEDB9594047E251B433F503998D270AA8792D0,
	LPWPosition__ctor_m8662EC582EBF81A2754433D0F7180284C3C09B8E,
	LPWPosition_GetHashCode_mFCCAE9479C211673BB8B64A152D7318A2920836C,
	LPWPosition_Equals_mD3AF450217DA095D13C93D58FBC129C0DAF76944,
	LPWReflectionParams__ctor_mF94FDFEA1B521D53A79694C4884BDF0F32F0D435,
	LPWReflection_Init_mFDE7AF47086F5B9A78E2210C028BB9C9A2179CDD,
	LPWReflection_OnWillRenderObject_mB3B075BB0BE6078E478A2081610DD7686830CA19,
	LPWReflection_IsSceneView2D_m6915790A4FEF18A1E86FB7D13087E332EFBB073E,
	LPWReflection_OnDisable_m4CE558F6D5253A071DFF0D5C9540F1A55D9592FB,
	LPWReflection_Destroy__m6CC10B4FEC92C46A44B04DDCDAFC5D8490BACA2B,
	LPWReflection_UpdateCameraModes_m40B5044A989124D8A48E1C8B832ADCE74DF7DCBD,
	LPWReflection_CreateWaterObjects_m6C8DBCFB8FA580F13E37968BEA446555AC5A3640,
	LPWReflection_GetWaterMode_m46B583D4F8353059BB8AEEFEF7DD3B8143BF04D3,
	LPWReflection_FindHardwareWaterSupport_mBE532D268454936CF7B9BAAB1746B7A8893E2E16,
	LPWReflection_CameraSpacePlane_m4AAA424BD7EEAB69CDAB69F9B9673825D469C409,
	LPWReflection_CalculateReflectionMatrix_m3B97D655D0C89BCC5C7139E223362E1E2C1C67F9,
	LPWReflection__ctor_m24834C7DF50FD6F643B16EB0549979340D15F367,
	LPWReflection__cctor_m08775F54D0A64635B698B175248303F1803D2A32,
	DemoFreeFlyCamera_Start_mCC3C8E63A79AF0F140FE20112B2664D2EC4CCBE3,
	DemoFreeFlyCamera_Update_m346AF785CC2FC87D9BBC7B7DCCE669D3C5ADE49A,
	DemoFreeFlyCamera__ctor_m36640884A06AC159ACB750CF32C92DA40AA74D26,
	ETFXLightFade_Start_m80EEDD11315E813676DA8E550C806F8C96374733,
	ETFXLightFade_Update_m3705B92646C8612BC70987DF39BC94B8F88EC49D,
	ETFXLightFade__ctor_mCDF41DC7334BC08F86DD3EF815FC1F3EA4C44F11,
	ETFXPitchRandomizer_Start_m64BE3B77C622908C2E324C6D3DFD17CB9092D21E,
	ETFXPitchRandomizer__ctor_m8493E0829884B225C1FC98134FA1EFCC44CDD238,
	ETFXRotation_Start_m3DC21DA9C26DBA1E14C27B9B997F46BEBBFB22C7,
	ETFXRotation_Update_mED37E3F435A6166C540A1DA30B49354F05279100,
	ETFXRotation__ctor_mC1C7849D64611953B026F069EE879C10B1399ABA,
};
extern void LPWPointDataEntry__ctor_m7D67D5B1C47F2585B63748B67013C0B6AFD038B8_AdjustorThunk (void);
extern void LPWPoint__ctor_m5E48D160398799B0900A51FC3D0BB606881FE745_AdjustorThunk (void);
extern void LPWPair__ctor_m246F718705CD2B394F03811F0763F093146167E7_AdjustorThunk (void);
extern void LPWPair_GetHashCode_mDFB5FBECB18F4A9AAD9C0475D6B0B7432D4847F5_AdjustorThunk (void);
extern void LPWPair_Equals_mD4DEDB9594047E251B433F503998D270AA8792D0_AdjustorThunk (void);
extern void LPWPosition__ctor_m8662EC582EBF81A2754433D0F7180284C3C09B8E_AdjustorThunk (void);
extern void LPWPosition_GetHashCode_mFCCAE9479C211673BB8B64A152D7318A2920836C_AdjustorThunk (void);
extern void LPWPosition_Equals_mD3AF450217DA095D13C93D58FBC129C0DAF76944_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[8] = 
{
	{ 0x06000076, LPWPointDataEntry__ctor_m7D67D5B1C47F2585B63748B67013C0B6AFD038B8_AdjustorThunk },
	{ 0x06000077, LPWPoint__ctor_m5E48D160398799B0900A51FC3D0BB606881FE745_AdjustorThunk },
	{ 0x06000078, LPWPair__ctor_m246F718705CD2B394F03811F0763F093146167E7_AdjustorThunk },
	{ 0x06000079, LPWPair_GetHashCode_mDFB5FBECB18F4A9AAD9C0475D6B0B7432D4847F5_AdjustorThunk },
	{ 0x0600007A, LPWPair_Equals_mD4DEDB9594047E251B433F503998D270AA8792D0_AdjustorThunk },
	{ 0x0600007B, LPWPosition__ctor_m8662EC582EBF81A2754433D0F7180284C3C09B8E_AdjustorThunk },
	{ 0x0600007C, LPWPosition_GetHashCode_mFCCAE9479C211673BB8B64A152D7318A2920836C_AdjustorThunk },
	{ 0x0600007D, LPWPosition_Equals_mD3AF450217DA095D13C93D58FBC129C0DAF76944_AdjustorThunk },
};
static const int32_t s_InvokerIndices[150] = 
{
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	1218,
	1478,
	1478,
	1478,
	1456,
	1456,
	1423,
	1208,
	1423,
	1208,
	1478,
	1478,
	1478,
	1478,
	1478,
	1472,
	1478,
	1478,
	1478,
	1423,
	1478,
	1478,
	1478,
	1423,
	1478,
	721,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	1218,
	1478,
	1478,
	1434,
	1434,
	1478,
	1478,
	1478,
	732,
	1478,
	1478,
	1423,
	721,
	721,
	1434,
	1478,
	2471,
	1478,
	842,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	2471,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	1236,
	1218,
	1478,
	1120,
	727,
	423,
	724,
	724,
	597,
	1152,
	431,
	1478,
	1478,
	1478,
	2471,
	1236,
	1478,
	1478,
	1218,
	1478,
	2471,
	2206,
	2408,
	2408,
	2242,
	2204,
	2407,
	2407,
	2415,
	2407,
	2415,
	2203,
	2197,
	1478,
	421,
	576,
	446,
	1254,
	652,
	652,
	652,
	1423,
	1100,
	1254,
	1423,
	1103,
	1478,
	429,
	1478,
	1456,
	1478,
	1218,
	724,
	406,
	1423,
	1423,
	235,
	2243,
	1478,
	2471,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
	1478,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	150,
	s_methodPointers,
	8,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

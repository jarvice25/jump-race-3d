using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants : MonoBehaviour
{
    public static Constants Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    int jumpForce = 1000, moveSpeed = 1000, rotateSpeed = 1000;

    string playerName;

    Rigidbody rb;
    Animator anim;


    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();


        playerName = PlayerNameGenrator.Instance.PlayerName;
        RankManager.Instance.AddInitialRank(playerName, 0);
        UIManager.Instance.UpdatePlayerRanks();

    }

    private void Update()
    {
        if (!GameManager.Instance.PlayerAlive())
        {
            return;
        }

    }

    private void FixedUpdate()
    {
        if (!GameManager.Instance.PlayerAlive() || GameManager.Instance.LevelCompleted() || GameManager.Instance.GameStart == GameStates.States.Pause)
        {
            return;
        }

        MovePlayer();

    }


    private void MovePlayer()
    {
        Vector2 movementValue = InputManager.Instance.GetInputValues();

        //rb.MoveRotation(rb.rotation * Quaternion.Euler(new Vector3(0, Input.GetAxis("Mouse X") * rotateSpeed * Time.fixedDeltaTime, 0)));
        rb.MovePosition(transform.position + (transform.forward * movementValue.y * moveSpeed) * Time.fixedDeltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!GameManager.Instance.PlayerAlive() || GameManager.Instance.LevelCompleted())
        {
            return;
        }

        if (collision.gameObject.tag == "Ground")
        {
            rb.velocity = Vector3.zero;
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);

            RankManager.Instance.UpdateRank(playerName, collision.gameObject.GetComponent<JumpFloor>().KeyValue);
            UIManager.Instance.UpdatePlayerRanks();
        }
        else if (collision.gameObject.tag == "Finish")
        {
            GameManager.Instance.GameState = GameStates.States.LevelCompleted;
            UIManager.Instance.ShowGameOver();
        }
        else if (collision.gameObject.tag == "Water")
        {
            GameManager.Instance.GameState = GameStates.States.GameOver;
        }
    }
}

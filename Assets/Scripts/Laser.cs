using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    LineRenderer lr;

    private void Start()
    {
        lr = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        RaycastHit hit;
        lr.SetPosition(0, transform.position);
        if (Physics.Raycast(transform.position, transform.up * -1, out hit))
        {
            if (hit.collider)
            {
                lr.SetPosition(1, new Vector3(0, hit.point.y, 0));
            }
        }
        else
        {
            lr.SetPosition(1, new Vector3(0, 5000, 0));
        }
    }
}

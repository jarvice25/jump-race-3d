using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpFloor : MonoBehaviour
{
    public static JumpFloor Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    int key;

    private void Start()
    {
        key = JumpFloorSpawner.Instance.GetJumpFloorKey;
    }


    public int KeyValue
    {
        get
        {
            return key;
        }

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    [SerializeField]
    GameObject currentPanel, gameStartPanel, gameOverPanel;

    [SerializeField]
    Button playBtn, restartBtn;

    [SerializeField]
    Text[] gameOverPlayers, currentPlayers;

    int enemyCompletedLevels=0;

    private void Start()
    {
        DeActivateAll();
        gameStartPanel.SetActive(true);

        playBtn.onClick.AddListener(() =>
        {
            DeActivateAll();
            currentPanel.SetActive(true);
            GameManager.Instance.GameStart = GameStates.States.Start;
        });

        restartBtn.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        });
    }

    void DeActivateAll()
    {
        currentPanel.SetActive(false);
        gameStartPanel.SetActive(false);
        gameOverPanel.SetActive(false);
    }

    public void ShowGameOver()
    {
        string[] players = new string[RankManager.Instance.getPlayerCount];
        players = RankManager.Instance.GetPlayersName();

        int i = 0;
        foreach (var item in players)
        {
            gameOverPlayers[i].text = item;
            i++;
        }


        DeActivateAll();
        gameOverPanel.SetActive(true);
    }

    public void UpdatePlayerRanks()
    {
        string[] players = new string[RankManager.Instance.getPlayerCount];
        players = RankManager.Instance.GetPlayersName();


        for (int j=0 + enemyCompletedLevels; j<players.Length; j++)
        {
            currentPlayers[j].text = players[j];

        }
    }

    public void LevelCompleedByPlayer()
    {
        enemyCompletedLevels++;
    }
}

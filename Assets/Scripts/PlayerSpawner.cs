using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    public static PlayerSpawner Instance;

    [SerializeField]
    GameObject enemyPlayer;

    [SerializeField]
    Transform[] position;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {

        for (int i = 0; i < position.Length; i++)
        {
            SpawnPlayer(enemyPlayer, position[i].position);
        }

    }

    void SpawnPlayer(GameObject _player, Vector3 _position)
    {
        _position.y = Random.Range(0.5f, 2f);
        Instantiate(_player, _position, Quaternion.identity);
    }

}

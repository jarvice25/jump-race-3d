using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNameGenrator : MonoBehaviour
{
    public static PlayerNameGenrator Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    string playerName = "Player";
    int index=0;

    public string GetPlayerName
    {
        get
        {
            index++;
            return playerName + index.ToString();
        }
    }

    public string PlayerName
    {
        get
        {
            index++;
            return "You";
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform targetPos;
    public float smoothSpeed = 0.125f, rotateSpeed = 100;

    Vector3 offset;


    private void Start()
    {
        offset = transform.position - targetPos.position;
    }

    private void Update()
    {
        
    }

    private void LateUpdate()
    {
        Vector3 desiredPosition = targetPos.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = smoothedPosition;

        //transform.rotation = Quaternion.Euler(new Vector3(0, Input.GetAxis("Mouse X") * rotateSpeed * Time.fixedDeltaTime, 0));
    }
}

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RankManager : MonoBehaviour
{
    public static RankManager Instance;


    Dictionary<string, int> rank = new Dictionary<string, int>();
    int playerCount=0;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public int getPlayerCount
    {
        get
        {
            return playerCount;
        }
    }

    public void AddInitialRank(string _name, int _value)
    {
        rank.Add(_name,_value);
        playerCount++;
    }

    public void UpdateRank(string _name, int _value)
    {
        rank[_name] = _value;
    }

    public string[] GetPlayersName()
    {
        string[] players = new string[playerCount];
        int i = 0;
        foreach (KeyValuePair<string, int> author in rank.OrderBy(key => key.Value))
        {
            players[i] = author.Key;
            i++;
            //print("Key : "+ author.Key +" - Value : "+author.Value);

        }
        //print("Start");
        //foreach (var item in players)
        //{
        //    print(item);
        //}
        //print("End");
        return players;
    }
}

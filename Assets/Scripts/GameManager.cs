using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    bool isPlayerAlive, isLevelCompleted;
    GameStates.States currentGameState, gameStarted;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        isPlayerAlive = true;
        isLevelCompleted = false;
        currentGameState = GameStates.States.Alive;
        gameStarted = GameStates.States.Pause;
    }

    public bool PlayerAlive()
    {
        return isPlayerAlive;
    }
    public bool LevelCompleted()
    {
        return isLevelCompleted;
    }

    public GameStates.States GameState
    {
        get
        {
            return currentGameState;
        }

        set
        {
            currentGameState = value;
            if (currentGameState == GameStates.States.GameOver)
            {
                isPlayerAlive = false;
                UIManager.Instance.ShowGameOver();
            }
            else if (currentGameState == GameStates.States.LevelCompleted)
            {
                isLevelCompleted = true;
            }
            
        }
    }

    public GameStates.States GameStart
    {
        get
        {
            return gameStarted;
        }
        set
        {
            gameStarted = value;
        }
    }

}

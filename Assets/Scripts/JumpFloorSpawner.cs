using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpFloorSpawner : MonoBehaviour
{
    public static JumpFloorSpawner Instance;

    public float interval;
    [SerializeField]
    int jumpFloorCount = 10;
    int jumpFloorKeyValue = 0;

    [SerializeField]
    Transform startPosition;

    [SerializeField]
    GameObject jumpFloor, lastFloor, parentObject;

    Vector3 tempPos;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        tempPos = startPosition.position;
        GenrateJumpFloors();
    }

    public int GetJumpFloorKey
    {
        get
        {
            return jumpFloorKeyValue++;
        }
    }


    void GenrateJumpFloors()
    {
        for (int i = 0; i < jumpFloorCount; i++)
        {
            GenrateFloor(jumpFloor, i);
        }
        GenrateFloor(lastFloor, jumpFloorCount);
    }

    void GenrateFloor(GameObject _jumpFloorObject, int _jumpFloorKeyValue)
    {
        GameObject temp =  Instantiate(_jumpFloorObject, tempPos, Quaternion.identity);
        temp.transform.SetParent(parentObject.transform);

        tempPos.z += interval;
    }
}

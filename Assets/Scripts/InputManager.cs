using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;

    string horizontalMovement = "Horizontal";
    string verticalMovement = "Vertical";

    Vector2 movement;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Update()
    {
        if (!GameManager.Instance.PlayerAlive() || GameManager.Instance.LevelCompleted())
        {
            return;
        }
        movement = new Vector2(Input.GetAxis(horizontalMovement), Input.GetAxis(verticalMovement));

        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Stationary)
            {
                movement.y = 1;
            }
        }
    }

    public Vector2 GetInputValues()
    {
        return movement;
    }

}

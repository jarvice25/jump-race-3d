using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public static Enemy Instance;

    [SerializeField]
    int jumpForce = 1000, moveSpeed = 1000;
    int jumpCount;


    float interval;

    bool isJumpCountStarted, isMovePlayer, isLevelCompleted, isPlayerAlive;

    string playerName;

    Vector3 nextMovePosition;


    Rigidbody rb;



    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        isLevelCompleted = false;
        isPlayerAlive = true;
        interval = JumpFloorSpawner.Instance.interval;

        nextMovePosition.z += interval;


        playerName = PlayerNameGenrator.Instance.GetPlayerName;
        RankManager.Instance.AddInitialRank(playerName, 0);
        UIManager.Instance.UpdatePlayerRanks();
    }

    private void FixedUpdate()
    {
        if (!GameManager.Instance.PlayerAlive() || GameManager.Instance.LevelCompleted() || GameManager.Instance.GameStart == GameStates.States.Pause || isLevelCompleted || !isPlayerAlive)
        {
            return;
        }

        if (transform.position.z >= nextMovePosition.z)
        {
            isMovePlayer = false;
            nextMovePosition.z += interval;
            return;
        }

        if (isMovePlayer)
        {
            MoveEnemyPlayer();
        }

    }

    private void MoveEnemyPlayer()
    {
        rb.MovePosition(transform.position + (transform.forward * moveSpeed) * Time.fixedDeltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!GameManager.Instance.PlayerAlive() || GameManager.Instance.LevelCompleted() || isLevelCompleted || !isPlayerAlive)
        {
            return;
        }

        if (collision.gameObject.tag == "Ground")
        {
            RankManager.Instance.UpdateRank(playerName, collision.gameObject.GetComponent<JumpFloor>().KeyValue);
            UIManager.Instance.UpdatePlayerRanks();
            if (jumpCount > 0)
            {
                jumpCount--;
            }
            else if (jumpCount == 0 && !isJumpCountStarted)
            {
                jumpCount = Random.Range(1, 4);
                isJumpCountStarted = true;
                isMovePlayer = false;
            }
            else if (jumpCount == 0 && isJumpCountStarted)
            {
                isMovePlayer = true;
                isJumpCountStarted = false;
            }
            rb.velocity = Vector3.zero;
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
        else if (collision.gameObject.tag == "Finish")
        {
            isLevelCompleted = true;
            UIManager.Instance.LevelCompleedByPlayer();
        }
        else if (collision.gameObject.tag == "Water")
        {
            isPlayerAlive = false;
        }
    }
}
